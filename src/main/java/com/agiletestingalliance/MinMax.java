package com.agiletestingalliance;

public class MinMax {

    public int checkMinMax(int paramA, int paramB) {
        if (paramB > paramA) {
            return paramB;
        } else {
            return paramA;
        }
    }

}
