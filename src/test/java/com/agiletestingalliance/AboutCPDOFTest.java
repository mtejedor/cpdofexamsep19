package com.agiletestingalliance;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class AboutCPDOFTest {

    @Test
    public void test() {
        AboutCPDOF aboutCPDOFTest = new AboutCPDOF();
        String result = aboutCPDOFTest.desc();
        assertTrue(result.startsWith("CP-DOF certification program covers end to end DevOps Life Cycle practically"));
    }
}
