package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class DurationTest {

    @Test
    public void test() {
        Duration duration = new Duration();
        String result = duration.dur();
        assertTrue(result.startsWith("CP-DOF is designed specifically for corporates"));
    }
}
