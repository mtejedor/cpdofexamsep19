package com.agiletestingalliance;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class MinMaxTest {

    @Test
    public void test() {
        MinMax minMax = new MinMax();
        int resultBMax = minMax.checkMinMax(1,2);
        assertTrue(resultBMax == 2);

        int resultAMax = minMax.checkMinMax(2,1);
        assertTrue(resultAMax == 2);
    }
}
