package com.agiletestingalliance;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class UsefulnessTest {

    @Test
    public void test() {
        Usefulness usefulness = new Usefulness();
        String result = usefulness.desc();
        assertTrue(result.startsWith("DevOps is about transformation"));
    }
}
